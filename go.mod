module gitlab.com/telo_tade/million_emails

go 1.15

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/go-log/log v0.2.0
	github.com/google/subcommands v1.2.0
	github.com/gorilla/mux v1.8.0
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.6.0
	github.com/streadway/amqp v1.0.0
	github.com/stretchr/testify v1.5.1
	github.com/thoas/go-funk v0.7.0
	gopkg.in/yaml.v2 v2.3.0
	gorm.io/driver/postgres v1.0.1
	gorm.io/gorm v1.20.1
)
