package main

import (
	"context"
	"flag"
	"os"

	"github.com/google/subcommands"
	"gitlab.com/telo_tade/million_emails/pkg/commands"
	configsreader "gitlab.com/telo_tade/million_emails/pkg/configs"
	"gitlab.com/telo_tade/million_emails/pkg/consumer"
	"gitlab.com/telo_tade/million_emails/pkg/db"
	"gitlab.com/telo_tade/million_emails/pkg/email"
	"gitlab.com/telo_tade/million_emails/pkg/generator"
	"gitlab.com/telo_tade/million_emails/pkg/inspector"
	"gitlab.com/telo_tade/million_emails/pkg/logger"
	"gitlab.com/telo_tade/million_emails/pkg/producer"
	"gitlab.com/telo_tade/million_emails/pkg/rest"
	"gitlab.com/telo_tade/million_emails/pkg/service"
)

// GitCommit is used to inject the commit number.
var GitCommit = "local_build"

// Version is used to inject the version number.
var Version = "unknown"

func main() {
	configs, err := configsreader.ReadConfigs()
	if err != nil {
		panic(err.Error())
	}

	l := logger.InitLogger()
	l.Logf("µS - commit: %v, version: %v", GitCommit, Version)

	db, err := db.NewClient(configs.DBHost, configs.DBPort, configs.Username, configs.Password, configs.Database)
	if err != nil {
		panic(err.Error())
	}

	service := service.Service{Logger: l, DBClient: db}
	handler := rest.Handler{Logger: l, Service: service}
	server := rest.NewServer(configs.Host, configs.Port, l, handler)
	gen := generator.Client{DB: db, Count: configs.GenerateSize}
	prod := producer.Client{
		DB: db, L: l, MaxCount: configs.MaxCount, StartID: configs.StartID, StopID: configs.StopID,
		QueueURLs: configs.QueueURLs,
	}
	inspect := inspector.Client{L: l, QueueURLs: configs.QueueURLs}

	email, err := email.New(configs.FailureRate, configs.TimeoutRate, configs.From)
	if err != nil {
		panic(err.Error())
	}

	consumer := consumer.Client{L: l, QueueURL: configs.ConsumerQueue, Sender: email}

	subcommands.Register(subcommands.HelpCommand(), "")
	subcommands.Register(subcommands.FlagsCommand(), "")
	subcommands.Register(subcommands.CommandsCommand(), "")

	subcommands.Register(&commands.API{
		Server: server,
	}, "")
	subcommands.Register(&commands.Generate{
		Generator: &gen,
	}, "")
	subcommands.Register(&commands.Produce{
		Producer: &prod,
	}, "")
	subcommands.Register(&commands.Inspect{
		Inspector: &inspect,
	}, "")
	subcommands.Register(&commands.Consume{
		Consumer: &consumer,
	}, "")

	flag.Parse()

	ctx := context.Background()
	os.Exit(int(subcommands.Execute(ctx)))
}
