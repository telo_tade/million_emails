/*
Package configsreader offers functionality to read config data from a yaml file. Its main methods is:
	ReadConfigs(filename string) (*Configs, error)
This function will open the yaml file, read its contents and returns a Cofnig struct with the values it has read.
*/
package configsreader

import (
	"flag"
	"fmt"
	"io/ioutil"
	"net/url"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
)

// Configs models the configuration values needed by the "Usage Collector" module.
type Configs struct {
	DBHost       string `json:"dbhost" yaml:"dbhost"`
	DBPort       uint   `json:"dbport" yaml:"dbport"`
	Username     string `json:"username" yaml:"username"`
	Password     string `json:"password" yaml:"password"`
	Database     string `json:"database" yaml:"database"`
	Host         string `json:"host" yaml:"host"`
	Port         uint   `json:"port" yaml:"port"`
	GenerateSize int    `json:"generateSize" yaml:"generateSize"`

	StartID       int      `json:"startID" yaml:"startID"`
	StopID        int      `json:"stopID" yaml:"stopID"`
	MaxCount      int      `json:"maxCount" yaml:"maxCount"`
	QueueURLs     []string `json:"queueURLs" yaml:"queueURLs"`
	ConsumerQueue string   `json:"consumerQueue" yaml:"consumerQueue"`
	FailureRate   int      `json:"failureRate" yaml:"failureRate"`
	TimeoutRate   int      `json:"timeoutRate" yaml:"timeoutRate"`
	From          string   `json:"from" yaml:"from"`
}

func (c Configs) String() string {
	return fmt.Sprintf("Configs[DBHost %v, DBPort %v, Username %v, "+
		"Database %v, Host %v, Port %v, GenerateSize %v, StartID %v, StopID %v, ConsumerQueue %v, "+
		"FailureRate %v, TimeoutRate %v, From %v]",
		c.DBHost, c.DBPort, c.Username, c.Database, c.Host, c.Port, c.GenerateSize, c.StartID, c.StopID,
		c.ConsumerQueue, c.FailureRate, c.TimeoutRate, c.From)
}

// ReadConfigs reads configs form a yaml file, returns a Cofnig struct with the values it has read.
func ReadConfigs() (*Configs, error) {
	filename := flag.String("config", "config/config.yaml", "Absolute or relative path to the config file")
	flag.Parse()

	yamlFile, err := ioutil.ReadFile(*filename)
	if err != nil {
		return nil, errors.Wrap(err, "Reading the config file at "+*filename+" failed")
	}

	result := Configs{}

	err = yaml.Unmarshal(yamlFile, &result)
	if err != nil {
		return nil, errors.Wrap(err, "Unmarshalling the content of the config file failed")
	}

	if len(result.QueueURLs) > 0 {
		for i := 0; i < len(result.QueueURLs); i++ {
			_, urlErr := url.ParseRequestURI(result.QueueURLs[i])

			if urlErr != nil {
				return nil, errors.New(fmt.Sprintf("Invalid URL: %v", result.QueueURLs[i]))
			}
		}
	}

	_, urlErr := url.ParseRequestURI(result.ConsumerQueue)

	if urlErr != nil {
		return nil, errors.New(fmt.Sprintf("Invalid URL: %v", result.ConsumerQueue))
	}

	return &result, nil
}
