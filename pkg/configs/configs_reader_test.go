package configsreader_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	configsreader "gitlab.com/telo_tade/million_emails/pkg/configs"
)

func TestReadConfigs(t *testing.T) {
	configs, err := configsreader.ReadConfigs()
	assert.Nil(t, configs, "Configs should be nil")

	expected := "Reading the config file at config/config.yaml failed: open config/config.yaml: no such file or directory"
	assert.Equal(t, expected, err.Error(), "Wrong error message.")
}
