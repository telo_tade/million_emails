package commands

import (
	"context"
	"flag"

	"github.com/google/subcommands"
)

// Produce represents a cmd line command.
type Produce struct {
	Producer executor
}

type executor interface {
	Execute()
}

// Name returns the name of the command.
func (p *Produce) Name() string { return "produce" }

// Synopsis returns a short string (less than one line) describing the command.
func (p *Produce) Synopsis() string { return "Start this app in producer mode." }

// Usage returns a long string explaining the command and giving usage information.
func (p *Produce) Usage() string {
	return "Start the app in producer mode. It will read Recipients from DB and add them to message queues.\n" +
		"The config variables <startID> and <stopID> define the (exclusive) range of recipients read.\n" +
		"<maxCount> decides how many recipients are read at once from DB. <queueURLs> store the message queues URLs.\n"
}

// SetFlags adds the flags for this command to the specified set.
func (p *Produce) SetFlags(_ *flag.FlagSet) {}

// Execute executes the command and returns an ExitStatus.
func (p *Produce) Execute(_ context.Context, _ *flag.FlagSet, _ ...interface{}) (exitStatus subcommands.ExitStatus) {
	p.Producer.Execute()

	return subcommands.ExitSuccess
}
