package commands

import (
	"context"
	"flag"

	"github.com/google/subcommands"
)

// Consume represents a cmd line command.
type Consume struct {
	Consumer consumer
}

type consumer interface {
	Execute()
}

// Name returns the name of the command.
func (c *Consume) Name() string { return "consume" }

// Synopsis returns a short string (less than one line) describing the command.
func (c *Consume) Synopsis() string { return "Start this app in consumer mode." }

// Usage returns a long string explaining the command and giving usage information.
func (c *Consume) Usage() string {
	return "Start the app in consumer mode. It will read emails form its queue and send them.\n" +
		"<queueURL> config variable stores the single message queue this consumer reads from.\n"
}

// SetFlags adds the flags for this command to the specified set.
func (c *Consume) SetFlags(_ *flag.FlagSet) {}

// Execute executes the command and returns an ExitStatus.
func (c *Consume) Execute(_ context.Context, _ *flag.FlagSet, _ ...interface{}) (exitStatus subcommands.ExitStatus) {
	c.Consumer.Execute()

	return subcommands.ExitSuccess
}
