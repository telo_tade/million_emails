package commands

import (
	"context"
	"flag"

	"github.com/google/subcommands"
)

// Inspect represents a cmd line command.
type Inspect struct {
	Inspector inspector
}

type inspector interface {
	Execute()
}

// Name returns the name of the command.
func (i *Inspect) Name() string { return "inspect" }

// Synopsis returns a short string (less than one line) describing the command.
func (i *Inspect) Synopsis() string { return "Start this app in inspector mode." }

// Usage returns a long string explaining the command and giving usage information.
func (i *Inspect) Usage() string {
	return "Start the app in inspector mode. It will periodically print info about the queues.\n" +
		"<queueURLs> config variable stores the message queues URLs.\n"
}

// SetFlags adds the flags for this command to the specified set.
func (i *Inspect) SetFlags(_ *flag.FlagSet) {}

// Execute executes the command and returns an ExitStatus.
func (i *Inspect) Execute(_ context.Context, _ *flag.FlagSet, _ ...interface{}) (exitStatus subcommands.ExitStatus) {
	i.Inspector.Execute()

	return subcommands.ExitSuccess
}
