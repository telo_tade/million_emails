package commands_test

import (
	"context"
	"flag"
	"testing"

	"github.com/google/subcommands"
	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/million_emails/pkg/commands"
)

type consumerMock struct {
	count int
}

func (c *consumerMock) Execute() {
	c.count++
}

func TestConsumerCmd_Execute(t *testing.T) {
	type fields struct {
		Consumer consumerMock
	}

	type args struct {
		in0 context.Context
		in1 *flag.FlagSet
		in2 []interface{}
	}

	tests := []struct {
		name           string
		fields         fields
		args           args
		wantExitStatus subcommands.ExitStatus
	}{
		{
			name: "Consume cmd should call Execute()",
			args: args{
				in0: context.Background(),
				in1: nil,
				in2: nil,
			},
			wantExitStatus: subcommands.ExitSuccess,
			fields:         fields{Consumer: consumerMock{}},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cmd := commands.Consume{
				Consumer: &tt.fields.Consumer,
			}

			result := cmd.Execute(tt.args.in0, tt.args.in1, tt.args.in2...)

			assert.Equal(t, tt.wantExitStatus, result, "Wrong result.")
			assert.Equal(t, 1, tt.fields.Consumer.count, "Execute should have been called.")
		})
	}
}
