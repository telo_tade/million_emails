package commands

import (
	"context"
	"flag"
	"os"
	"os/signal"
	"syscall"

	"github.com/google/subcommands"
)

// API represents a cmd line command.
type API struct {
	Server starter
}

type starter interface {
	Start()
}

// Name returns the name of the command.
func (a *API) Name() string { return "api" }

// Synopsis returns a short string (less than one line) describing the command.
func (a *API) Synopsis() string { return "Start this app in API mode." }

// Usage returns a long string explaining the command and giving usage information.
func (a *API) Usage() string {
	return "Start the app in API mode. This will start a server which provides a rest API.\n" +
		"Its purpose is to manipulate a Recipient object, offering CRUD functionality around a DB table."
}

// SetFlags adds the flags for this command to the specified set.
func (a *API) SetFlags(_ *flag.FlagSet) {}

// Execute executes the command and returns an ExitStatus.
func (a *API) Execute(_ context.Context, _ *flag.FlagSet, _ ...interface{}) (exitStatus subcommands.ExitStatus) {
	a.Server.Start()

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGTERM)
	<-sig

	return subcommands.ExitSuccess
}
