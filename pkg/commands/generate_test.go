package commands_test

import (
	"context"
	"flag"
	"testing"

	"github.com/google/subcommands"
	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/million_emails/pkg/commands"
)

type generatorMock struct {
	count int
}

func (g *generatorMock) Generate() error {
	g.count++

	return nil
}

func TestGenerateCmd_Execute(t *testing.T) {
	fs := flag.NewFlagSet("TestValue", flag.ExitOnError)
	count := 1000000
	fs.IntVar(&count, "count", count, "How many recipients should be generated.")

	type fields struct {
		Generator *generatorMock
	}

	type args struct {
		in0 context.Context
		in1 *flag.FlagSet
		in2 []interface{}
	}

	tests := []struct {
		name           string
		fields         fields
		args           args
		wantExitStatus subcommands.ExitStatus
	}{
		{
			name: "Generate cmd should call Generate()",
			args: args{
				in0: context.Background(),
				in1: fs,
				in2: nil,
			},
			wantExitStatus: subcommands.ExitSuccess,
			fields:         fields{Generator: &generatorMock{}},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cmd := commands.Generate{
				Generator: tt.fields.Generator,
			}

			result := cmd.Execute(tt.args.in0, tt.args.in1, tt.args.in2...)

			assert.Equal(t, tt.wantExitStatus, result, "Wrong result.")
			assert.Equal(t, 1, tt.fields.Generator.count, "Generate should have been called.")
		})
	}
}
