package commands

import (
	"context"
	"flag"

	"github.com/google/subcommands"
)

// Generate represents a cmd line command.
type Generate struct {
	Generator generator
}

type generator interface {
	Generate() error
}

// Name returns the name of the command.
func (g *Generate) Name() string { return "generate" }

// Synopsis returns a short string (less than one line) describing the command.
func (g *Generate) Synopsis() string { return "Start this app in generator mode." }

// Usage returns a long string explaining the command and giving usage information.
func (g *Generate) Usage() string {
	return "Start the app in generator mode. This will cause it to create and save <count> Recipient objects.\n" +
		"The config variable <count> will determine the amount of objects created.\n" +
		"If N objects already exist in DB, <count> more will be created, for a total of N + <count>."
}

// SetFlags adds the flags for this command to the specified set.
func (g *Generate) SetFlags(_ *flag.FlagSet) {}

// Execute executes the command and returns an ExitStatus.
func (g *Generate) Execute(_ context.Context, _ *flag.FlagSet, _ ...interface{}) (exitStatus subcommands.ExitStatus) {
	err := g.Generator.Generate()
	if err != nil {
		return subcommands.ExitFailure
	}

	return subcommands.ExitSuccess
}
