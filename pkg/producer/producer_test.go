package producer_test

import (
	"testing"

	"github.com/streadway/amqp"
	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/million_emails/pkg/producer"
)

func TestCountNonNil(t *testing.T) {
	type args struct {
		queues []*amqp.Queue
	}

	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Standard slice",
			args: args{
				queues: []*amqp.Queue{{}, nil, nil, {}, nil},
			},
			want: 2,
		},
		{
			name: "Slice of nils",
			args: args{
				queues: []*amqp.Queue{nil, nil, nil, nil, nil},
			},
			want: 0,
		},
		{
			name: "No nils",
			args: args{
				queues: []*amqp.Queue{{}, {}, {}, {}},
			},
			want: 4,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := producer.CountNonNil(tt.args.queues); got != tt.want {
				t.Errorf("CountNonNil() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDistributeWork(t *testing.T) {
	type args struct {
		channels []*amqp.Channel
		queues   []*amqp.Queue
	}

	tests := []struct {
		name string
		args args
	}{
		{
			name: "One non nil",
			args: args{
				channels: []*amqp.Channel{{}, {}, {}, {}},
				queues:   []*amqp.Queue{{}, nil, nil, nil},
			},
		},
		{
			name: "Nils and non nils",
			args: args{
				channels: []*amqp.Channel{{}, {}, {}, {}, {}},
				queues:   []*amqp.Queue{{}, nil, nil, nil, {}},
			},
		},
		{
			name: "One element",
			args: args{
				channels: []*amqp.Channel{{}},
				queues:   []*amqp.Queue{{}},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result := producer.DistributeWork(tt.args.channels, tt.args.queues)

			assert.True(t, result >= 0, "Result should be positive.")
			assert.True(t, result < len(tt.args.channels), "Result should not be out of bounds.")
			assert.True(t, tt.args.channels[result] != nil, "Result should correspond to a non nil queue.")
		})
	}
}
