package producer

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"strconv"
	"time"

	"github.com/go-log/log"
	"github.com/streadway/amqp"
	"gitlab.com/telo_tade/million_emails/pkg/models"
)

const queueName = "million_emails"

// Client encapsulates prudcer functionality.
type Client struct {
	DB                        database
	L                         log.Logger
	QueueURLs                 []string
	StartID, StopID, MaxCount int
}

type database interface {
	GetNextUnnotified(ctx context.Context, startID, stopID, maxCount int) ([]models.Recipient, error)
	UpdateLastNotifiedBulk(IDs []string, when time.Time) error
}

func (c *Client) createConnections() []*amqp.Connection {
	result := make([]*amqp.Connection, 0, len(c.QueueURLs))

	for i := 0; i < len(c.QueueURLs); i++ {
		conn, err := amqp.Dial(c.QueueURLs[i])
		if err != nil {
			c.L.Logf("Error creating connection for %v: %v", c.QueueURLs[i], err.Error())
		} else {
			result = append(result, conn)
		}
	}

	return result
}

func (c *Client) closeConnections(input []*amqp.Connection) {
	for i := 0; i < len(input); i++ {
		input[i].Close()
	}
}

func (c *Client) createChannels(input []*amqp.Connection) []*amqp.Channel {
	result := make([]*amqp.Channel, 0, len(input))

	for i := 0; i < len(input); i++ {
		amqpChannel, err := input[i].Channel()
		if err != nil {
			c.L.Logf("Error creating channel for connection %v", i)
		} else {
			result = append(result, amqpChannel)
		}
	}

	return result
}

func (c *Client) closeChannels(input []*amqp.Channel) {
	for i := 0; i < len(input); i++ {
		input[i].Close()
	}
}

func (c *Client) declareQueue(input []*amqp.Channel) []*amqp.Queue {
	result := make([]*amqp.Queue, len(input))

	for i := 0; i < len(input); i++ {
		q, err := input[i].QueueDeclare(queueName, true, false, false, false, nil)
		if err != nil {
			c.L.Logf("Error declaring queue: %v", err.Error())
		} else {
			result[i] = &q
		}
	}

	return result
}

// Execute reads from DB and adds to the queues.
func (c *Client) Execute() {
	start := time.Now()
	defer c.logDuration(start)
	c.L.Logf("Starting to execute producer cmd: %v", start)

	connections := c.createConnections()
	defer c.closeConnections(connections)

	amqpChannels := c.createChannels(connections)
	defer c.closeChannels(amqpChannels)

	queues := c.declareQueue(amqpChannels)

	payload, dbErr := c.DB.GetNextUnnotified(context.Background(), c.StartID, c.StopID, c.MaxCount)
	if dbErr != nil {
		c.L.Logf("Error GetNextUnnotified %v", dbErr.Error())
	}

	for dbErr == nil && len(payload) > 0 {
		for i := 0; i < len(payload); i++ {
			queueIndex := DistributeWork(amqpChannels, queues)

			email := composeEmail(payload[i])

			body, err := json.Marshal(email)
			if err != nil {
				c.L.Logf("Error marshalling %v: %v", email, err.Error())
			}

			err = amqpChannels[queueIndex].Publish("", queues[queueIndex].Name, false, false, amqp.Publishing{
				DeliveryMode: amqp.Persistent,
				ContentType:  "text/plain",
				Body:         body,
			})

			if err != nil {
				c.L.Logf("Error publishing message: %v", err.Error())
			}
		}

		dbErr = c.DB.UpdateLastNotifiedBulk(extractIDs(payload), time.Now())
		if dbErr != nil {
			c.L.Logf("Error UpdateLastNotifiedBulk %v", dbErr.Error())
		}

		payload, dbErr = c.DB.GetNextUnnotified(context.Background(), c.StartID, c.StopID, c.MaxCount)
		if dbErr != nil {
			c.L.Logf("Error GetNextUnnotified %v", dbErr.Error())
		}
	}
}

func composeEmail(r models.Recipient) models.Email {
	return models.Email{
		Body:    fmt.Sprintf("Email body, meant to be sent to user with ID %v", r.ID),
		Subject: "Email for " + r.Name,
		To:      r.Email,
	}
}

func extractIDs(in []models.Recipient) []string {
	result := make([]string, len(in))

	for i := 0; i < len(in); i++ {
		result[i] = strconv.Itoa(in[i].ID)
	}

	return result
}

// DistributeWork returns the channel index which should be added more work to.
// Channels with nil queues are disqualified.
func DistributeWork(channels []*amqp.Channel, queues []*amqp.Queue) int {
	active := CountNonNil(queues)
	index := rand.Intn(active)

	result := 0
	for index > 0 {
		result++

		if queues[result] != nil {
			index--
		}
	}

	return result
}

// CountNonNil counts non nil objects.
func CountNonNil(queues []*amqp.Queue) int {
	result := 0

	for i := 0; i < len(queues); i++ {
		if queues[i] != nil {
			result++
		}
	}

	return result
}

func (c *Client) logDuration(start time.Time) {
	c.L.Logf("Started at %v, It took %v", start, time.Since(start))
}
