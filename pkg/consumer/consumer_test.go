// +build !race

package consumer_test

import (
	"testing"
	"time"

	"github.com/go-log/log"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/million_emails/pkg/consumer"
	"gitlab.com/telo_tade/million_emails/pkg/models"
)

type publisherMock struct {
	count int
}

func (p *publisherMock) Publish(exchange, key string, mandatory, immediate bool, msg amqp.Publishing) error {
	p.count++

	return nil
}

type emailsender interface {
	SendEmail(email models.Email) (err error)
	Count() int
}

type emailsenderMock struct {
	count int
}

func (e *emailsenderMock) SendEmail(email models.Email) (err error) {
	e.count++

	return nil
}

func (e *emailsenderMock) Count() int {
	return e.count
}

type emailsenderFailMock struct {
	count int
}

func (e *emailsenderFailMock) SendEmail(email models.Email) (err error) {
	e.count++

	return errors.Errorf("Mock error")
}

func (e *emailsenderFailMock) Count() int {
	return e.count
}

func TestClient_ProcessEmails(t *testing.T) {
	type fields struct {
		L        log.Logger
		QueueURL string
		Sender   emailsender
	}

	type args struct {
		messageChannel chan amqp.Delivery
		amqpChannel    *publisherMock
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		publish int
		sent    int
	}{
		{
			name:    "All emails send successfully",
			args:    args{messageChannel: make(chan amqp.Delivery), amqpChannel: &publisherMock{}},
			fields:  fields{L: t, QueueURL: "", Sender: &emailsenderMock{}},
			publish: 0,
			sent:    3,
		},
		{
			name:    "All emails fail to be sent",
			args:    args{messageChannel: make(chan amqp.Delivery), amqpChannel: &publisherMock{}},
			fields:  fields{L: t, QueueURL: "", Sender: &emailsenderFailMock{}},
			publish: 3,
			sent:    3,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := consumer.Client{L: tt.fields.L, QueueURL: tt.fields.QueueURL, Sender: tt.fields.Sender}

			go c.ProcessEmails(tt.args.messageChannel, tt.args.amqpChannel)

			email := `{"To":"user_842@mockserver.com","Subject":"Email for name_842","Body":` +
				`"Email body, meant to be sent to user with ID 842"}`

			tt.args.messageChannel <- amqp.Delivery{Body: []byte(email)}
			tt.args.messageChannel <- amqp.Delivery{Body: []byte(email)}
			tt.args.messageChannel <- amqp.Delivery{Body: []byte(email)}

			close(tt.args.messageChannel)

			time.Sleep(1500 * time.Millisecond)

			assert.Equal(t, tt.publish, tt.args.amqpChannel.count, "Publish should have been called %d times.", tt.publish)
			assert.Equal(t, tt.sent, tt.fields.Sender.Count(), "Send() should have been called %d times.", tt.sent)
		})
	}
}
