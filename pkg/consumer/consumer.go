package consumer

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/go-log/log"
	"github.com/streadway/amqp"
	"gitlab.com/telo_tade/million_emails/pkg/models"
)

const queueName = "million_emails"

// Client encapsulates functionality to read emails from a message queue and deliver them.
type Client struct {
	L        log.Logger
	QueueURL string
	Sender   emailsender
}

type emailsender interface {
	SendEmail(email models.Email) (err error)
}

// Execute reads from a queue and delivers.
func (c *Client) Execute() {
	start := time.Now()
	defer c.logDuration(start)
	c.L.Logf("Starting to execute producer cmd: %v", start)

	connection := c.createConnection()
	if connection == nil {
		c.L.Logf("Could not create connection.")

		return
	}
	defer connection.Close()

	amqpChannel := c.createChannel(connection)
	if amqpChannel == nil {
		c.L.Logf("Could not create amqpChannel channel.")

		return
	}
	defer amqpChannel.Close()

	messageChannel := c.createMessageChannel(amqpChannel)

	if messageChannel == nil {
		c.L.Logf("Could not create a message channel.")

		return
	}

	c.ProcessEmails(*messageChannel, amqpChannel)
}

type publisher interface {
	Publish(exchange, key string, mandatory, immediate bool, msg amqp.Publishing) error
}

// ProcessEmails reads emails from the queue and tries to send them.
func (c *Client) ProcessEmails(messageChannel <-chan amqp.Delivery, amqpChannel publisher) {
	e := models.Email{}

	for m := range messageChannel {
		fmt.Println("m.body", string(m.Body))

		err := json.Unmarshal(m.Body, &e)
		if err != nil {
			c.L.Logf("Error unmarshalling: %v.", err.Error())

			continue
		}

		err = m.Ack(false)
		if err != nil {
			c.L.Logf("Error acknowledging email %+v: %v.", e, err.Error())
		}

		err = c.Sender.SendEmail(e)
		if err != nil {
			c.L.Logf("Error sending email %+v: %v.", e, err.Error())

			err = amqpChannel.Publish("", queueName, false, false, amqp.Publishing{
				DeliveryMode: amqp.Persistent,
				ContentType:  "text/plain",
				Body:         m.Body,
			})

			if err != nil {
				c.L.Logf("Error publishing back unsent email %+v: %v.", e, err.Error())
			} else {
				c.L.Logf("Email %+v was placed back into the queue.", e)
			}
		} else {
			c.L.Logf("Email successfully sent: %+v.", e)
		}
	}
}

func (c *Client) createMessageChannel(input *amqp.Channel) *<-chan amqp.Delivery {
	if input == nil {
		return nil
	}

	err := input.Qos(1, 0, false)
	if err != nil {
		c.L.Logf("Error configuring channel: %v", err.Error())
	}

	messageChannel, err := input.Consume(
		queueName,
		"",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		c.L.Logf("Could not register consumer: %v", err.Error())
	}

	return &messageChannel
}

func (c *Client) createConnection() *amqp.Connection {
	conn, err := amqp.Dial(c.QueueURL)
	if err != nil {
		c.L.Logf("Error creating connection for %v: %v", c.QueueURL, err.Error())

		return nil
	}

	return conn
}

func (c *Client) createChannel(input *amqp.Connection) *amqp.Channel {
	if input == nil {
		return nil
	}

	amqpChannel, err := input.Channel()
	if err != nil {
		c.L.Logf("Error creating channel for connection %v", err.Error())

		return nil
	}

	return amqpChannel
}

func (c *Client) logDuration(start time.Time) {
	c.L.Logf("Started at %v, It took %v", start, time.Since(start))
}
