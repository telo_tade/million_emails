package models

import (
	"fmt"
	"regexp"
)

var emailRegexp = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+" +
	"@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

// Email models the info sent by an email.
type Email struct {
	To            string
	Subject, Body string
}

func (e Email) String() string {
	return fmt.Sprintf("Email[To %v, Subject %v, len(body) %v]", e.To, e.Subject, len(e.Body))
}

// Valid checks if the To field consist of three parts: {username, @, domain name}.
func (e *Email) Valid() bool {
	if len(e.Subject) == 0 || len(e.Body) == 0 {
		return false
	}

	return emailRegexp.MatchString(e.To)
}
