package models

import (
	"fmt"
	"time"
)

// Recipient models a user, as needed by the DB layer.
type Recipient struct {
	ID           int       `json:"id" gorm:"primary_key;autoIncrement"`
	Name         string    `json:"name"`
	Email        string    `json:"email"`
	LastNotified time.Time `json:"last_notified"`
}

func (r Recipient) String() string {
	return fmt.Sprintf("Recipient[ID %v, Name %v, Email %v, LastNotified %v]", r.ID, r.Name, r.Email, r.LastNotified)
}
