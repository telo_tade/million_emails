package models_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/million_emails/pkg/models"
)

func TestEmail_Valid(t *testing.T) {
	type fields struct {
		To      string
		Subject string
		Body    string
	}

	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name:   "Valid email",
			fields: fields{Body: "test body", Subject: "test subject", To: "test@gmail.com"},
			want:   true,
		},
		{
			name:   "Empty body",
			fields: fields{Body: "", Subject: "test subject", To: "test@gmail.com"},
			want:   false,
		},
		{
			name:   "Empty subject",
			fields: fields{Body: "test body", Subject: "", To: "test@gmail.com"},
			want:   false,
		},
		{
			name:   "Invalid email: no user",
			fields: fields{Body: "test body", Subject: "test subject", To: "@gmail.com"},
			want:   false,
		},
		{
			name:   "Invalid email: wrong domain",
			fields: fields{Body: "test body", Subject: "test subject", To: "@gma.il.com"},
			want:   false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := models.Email{
				To:      tt.fields.To,
				Subject: tt.fields.Subject,
				Body:    tt.fields.Body,
			}

			assert.Equal(t, tt.want, e.Valid(), "Invalid result.")
		})
	}
}
