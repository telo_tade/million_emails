package rest

import (
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"github.com/go-log/log"
	"github.com/gorilla/mux"
	"gitlab.com/telo_tade/million_emails/pkg/models"
	"gitlab.com/telo_tade/million_emails/pkg/service"
)

const layout = "2006-01-02T15:04:05.000Z"

// Handler stores common objects needed by all handlers.
type Handler struct {
	Logger  log.Logger
	Service service.Service
}

// GetAllRecipients returns a recipient by its ID.
func (h *Handler) GetAllRecipients(w http.ResponseWriter, r *http.Request) {
	now := time.Now()
	defer h.logDuration(now)
	h.Logger.Logf("GetAllRecipients: %v", now)

	recipients, err := h.Service.GetAllRecipients(r.Context())
	if err != nil {
		h.Logger.Logf("Could not retrieve all recipients: %v.", err)
		w.WriteHeader(http.StatusNotFound)

		return
	}

	h.writeJSONResponse(recipients, http.StatusOK, w)
}

// GetRecipient returns a recipient by its ID.
func (h *Handler) GetRecipient(w http.ResponseWriter, r *http.Request) {
	now := time.Now()
	defer h.logDuration(now)
	h.Logger.Logf("GetRecipient: %v", now)

	params := mux.Vars(r)
	recipientIDParam, ok := params["ID"]

	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		h.Logger.Logf("Request does not contain a valid recipient ID parameter.")

		return
	}

	recipientID, err := strconv.Atoi(recipientIDParam)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.Logger.Logf("Request does not contain a valid recipient ID parameter.")

		return
	}

	recipient, err := h.Service.GetRecipient(r.Context(), uint(recipientID))
	if err != nil {
		h.Logger.Logf("Could not retriver recipient with ID %v: %v.", recipientIDParam, err)
		w.WriteHeader(http.StatusNotFound)

		return
	}

	h.writeJSONResponse(recipient, http.StatusOK, w)
}

// CreateRecipient creates a new recipient.
func (h *Handler) CreateRecipient(w http.ResponseWriter, r *http.Request) {
	now := time.Now()
	defer h.logDuration(now)
	h.Logger.Logf("CreateRecipient: %v", now)

	rec := models.Recipient{}

	err := json.NewDecoder(r.Body).Decode(&rec)
	if err != nil {
		h.Logger.Logf("Could not decode recipient: %v", err)
		w.WriteHeader(http.StatusBadRequest)

		return
	}

	err = h.Service.CreateRecipient(r.Context(), rec)
	if err != nil {
		h.Logger.Logf("Could not create recipient: %v.", err)
		w.WriteHeader(http.StatusBadRequest)

		return
	}

	h.writeJSONResponse(rec, http.StatusCreated, w)
}

// HealthCheck returns "pong".
func (h *Handler) HealthCheck(w http.ResponseWriter, r *http.Request) {
	if _, err := w.Write([]byte("pong")); err != nil {
		w.WriteHeader(http.StatusInternalServerError)

		h.Logger.Logf("error writing response body, %v", err)
	}
}

func (h *Handler) writeJSONResponse(resp interface{}, code int, w http.ResponseWriter) {
	w.WriteHeader(code)
	w.Header().Set("Content-Type", "application/json")

	body, err := json.Marshal(resp)
	if err != nil {
		h.Logger.Logf("error when marshalling result to json, %v", err)
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	_, err = w.Write(body)
	if err != nil {
		h.Logger.Logf("error writing response body, %v", err)
		w.WriteHeader(http.StatusInternalServerError)

		return
	}
}

func (h *Handler) logDuration(start time.Time) {
	h.Logger.Logf("Started at %v, It took %v", start, time.Since(start))
}

// UpdateLastNotified updates the last notified date of a recipient.
func (h *Handler) UpdateLastNotified(w http.ResponseWriter, r *http.Request) {
	now := time.Now()
	defer h.logDuration(now)
	h.Logger.Logf("UpdateLastNotified: %v", now)

	params := mux.Vars(r)

	ID, ok := params["ID"]
	if !ok {
		h.Logger.Logf("ID not provided as argument")
		w.WriteHeader(http.StatusBadRequest)

		return
	}

	recipientID, err := strconv.Atoi(ID)
	if err != nil {
		h.Logger.Logf("ID is not a valid integer: %v", err)
		w.WriteHeader(http.StatusBadRequest)

		return
	}

	lastNotified, ok := params["lastNotified"]
	if !ok {
		h.Logger.Logf("LastNotified not provided as argument")
		w.WriteHeader(http.StatusBadRequest)

		return
	}

	lastNot, err := time.Parse(layout, lastNotified)
	if err != nil {
		h.Logger.Logf("lastNotified has invalid day format: %v", lastNotified)

		w.WriteHeader(http.StatusBadRequest)

		_, err := w.Write([]byte("lastNotified has invalid day format, should be: " + layout))
		if err != nil {
			h.Logger.Logf("error writing response %v", err.Error())
		}

		return
	}

	err = h.Service.UpdateLastNotified(r.Context(), recipientID, lastNot)
	if err != nil {
		h.Logger.Logf("Could not create recipient: %v.", err)
		w.WriteHeader(http.StatusBadRequest)

		return
	}

	h.writeJSONResponse("Success", http.StatusOK, w)
}

// DeleteRecipient delete the recipient with a given ID.
func (h *Handler) DeleteRecipient(w http.ResponseWriter, r *http.Request) {
	now := time.Now()
	defer h.logDuration(now)
	h.Logger.Logf("DeleteRecipient: %v", now)

	params := mux.Vars(r)
	recipientIDParam, ok := params["ID"]

	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		h.Logger.Logf("Request does not contain a valid recipient ID parameter.")

		return
	}

	recipientID, err := strconv.Atoi(recipientIDParam)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		h.Logger.Logf("Request does not contain a valid recipient ID parameter.")

		return
	}

	err = h.Service.DeleteRecipient(r.Context(), recipientID)
	if err != nil {
		h.Logger.Logf("Could not delete recipient with ID %v: %v.", recipientIDParam, err)
		w.WriteHeader(http.StatusNotFound)

		return
	}

	h.writeJSONResponse("Success", http.StatusOK, w)
}
