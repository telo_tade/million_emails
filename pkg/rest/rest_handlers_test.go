package rest_test

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
	"time"

	"github.com/go-log/log"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/million_emails/pkg/models"
	"gitlab.com/telo_tade/million_emails/pkg/rest"
	"gitlab.com/telo_tade/million_emails/pkg/service"
)

var testRecipient = models.Recipient{
	ID:           23,
	LastNotified: time.Date(2020, 10, 9, 8, 7, 6, 5, time.UTC),
	Email:        "john.doe@gmail.com",
	Name:         "John Doe",
}

type dbClientMock struct {
	getRecipient, lastNotified, deleteRecipient int
}

func (d *dbClientMock) GetRecipient(ctx context.Context, id uint) (models.Recipient, error) {
	d.getRecipient++

	return testRecipient, nil
}

func (d *dbClientMock) GetAllRecipients(ctx context.Context) ([]models.Recipient, error) {
	return []models.Recipient{}, nil
}

func (d *dbClientMock) CreateRecipient(ctx context.Context, p models.Recipient) error {
	return nil
}

func (d *dbClientMock) UpdateLastNotified(ctx context.Context, recipientID int, deletionAt time.Time) error {
	d.lastNotified++

	return nil
}

func (d *dbClientMock) DeleteRecipient(ctx context.Context, r models.Recipient) error {
	d.deleteRecipient++

	return nil
}

func TestHandler_GetRecipient(t *testing.T) {
	req, err := http.NewRequestWithContext(context.Background(), "GET", "localhost:8080/recipient", nil)
	assert.Nil(t, err, "Error creating request: %v.", err)

	req = mux.SetURLVars(req, map[string]string{
		"ID": "22",
	})

	type fields struct {
		Logger   log.Logger
		DBClient *dbClientMock
	}

	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "Returns mocked object",
			args: args{
				w: httptest.NewRecorder(),
				r: req,
			},
			fields: fields{
				Logger:   t,
				DBClient: &dbClientMock{},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := rest.Handler{
				Logger:  tt.fields.Logger,
				Service: service.Service{Logger: tt.fields.Logger, DBClient: tt.fields.DBClient},
			}

			h.GetRecipient(tt.args.w, tt.args.r)

			result := tt.args.w.(*httptest.ResponseRecorder).Result()
			assert.NotNil(t, result, "Response should not be nil.")
			assert.Equal(t, http.StatusOK, result.StatusCode, "Status code should be http.StatusOK.")

			defer result.Body.Close()

			body, err := ioutil.ReadAll(result.Body)
			assert.Nil(t, err, "readAll should produce no error: %v", err)
			assert.Equal(t, 1, tt.fields.DBClient.getRecipient, "GetRecipient BD call should be called once.")

			var actual models.Recipient
			err = json.Unmarshal(body, &actual)
			assert.Nil(t, err, "Error should be nil.")
			assert.True(t, reflect.DeepEqual(testRecipient, actual), "Wrong result value.")
		})
	}
}

func TestHandler_UpdateLastNotified(t *testing.T) {
	req, err := http.NewRequestWithContext(context.Background(), "POST", "localhost:8080/recipient", nil)
	assert.Nil(t, err, "Error creating request: %v.", err)

	req = mux.SetURLVars(req, map[string]string{
		"ID":           "22",
		"lastNotified": "2020-09-24T15:04:05.000Z",
	})

	type fields struct {
		Logger   log.Logger
		DBClient *dbClientMock
	}

	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "Returns success",
			args: args{
				w: httptest.NewRecorder(),
				r: req,
			},
			fields: fields{
				Logger:   t,
				DBClient: &dbClientMock{},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := rest.Handler{
				Logger:  tt.fields.Logger,
				Service: service.Service{Logger: tt.fields.Logger, DBClient: tt.fields.DBClient},
			}

			h.UpdateLastNotified(tt.args.w, tt.args.r)

			result := tt.args.w.(*httptest.ResponseRecorder).Result()
			assert.NotNil(t, result, "Response should not be nil.")
			assert.Equal(t, http.StatusOK, result.StatusCode, "Status code should be http.StatusOK.")

			defer result.Body.Close()

			body, err := ioutil.ReadAll(result.Body)
			assert.Nil(t, err, "readAll should produce no error: %v", err)
			assert.Equal(t, "\"Success\"", string(body), "Wrong response message")
			assert.Equal(t, 1, tt.fields.DBClient.lastNotified, "UpdateLastNotified DB call should be called once.")
		})
	}
}

func TestHandler_DeleteRecipient(t *testing.T) {
	req, err := http.NewRequestWithContext(context.Background(), "DELETE", "localhost:8080/recipient", nil)
	assert.Nil(t, err, "Error creating request: %v.", err)

	req = mux.SetURLVars(req, map[string]string{
		"ID": "22",
	})

	type fields struct {
		Logger   log.Logger
		DBClient *dbClientMock
	}

	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "Calls DB delete once",
			args: args{
				w: httptest.NewRecorder(),
				r: req,
			},
			fields: fields{
				Logger:   t,
				DBClient: &dbClientMock{},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := rest.Handler{
				Logger:  tt.fields.Logger,
				Service: service.Service{Logger: tt.fields.Logger, DBClient: tt.fields.DBClient},
			}

			h.DeleteRecipient(tt.args.w, tt.args.r)

			result := tt.args.w.(*httptest.ResponseRecorder).Result()
			assert.NotNil(t, result, "Response should not be nil.")
			assert.Equal(t, http.StatusOK, result.StatusCode, "Status code should be http.StatusOK.")

			defer result.Body.Close()

			body, err := ioutil.ReadAll(result.Body)
			assert.Nil(t, err, "readAll should produce no error: %v", err)
			assert.Equal(t, "\"Success\"", string(body), "Wrong response message")
			assert.Equal(t, 1, tt.fields.DBClient.deleteRecipient, "DeleteRecipient DB call should be called once.")
		})
	}
}
