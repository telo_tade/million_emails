package rest

import (
	"fmt"
	"net/http"

	"github.com/go-log/log"
	"github.com/gorilla/mux"
)

// Server represents a REST API instance.
type Server struct {
	Logger  log.Logger
	Router  *mux.Router
	Handler Handler
	Host    string
	Port    uint
}

// NewServer creates a new server.
func NewServer(host string, port uint, log log.Logger, handler Handler) *Server {
	s := &Server{
		Router:  mux.NewRouter(),
		Logger:  log,
		Handler: handler,
		Host:    host,
		Port:    port,
	}

	s.InitRoutes()

	return s
}

// Start launches a new goroutine which calls ListenAndServe.
func (s *Server) Start() {
	go func() {
		err := http.ListenAndServe(fmt.Sprintf("%v:%v", s.Host, s.Port), s.Router)

		s.Logger.Logf("Error running the server: %v", err)
	}()
}

// InitRoutes configures paths and handlers.
func (s *Server) InitRoutes() {
	s.Router.HandleFunc("/ping", s.Handler.HealthCheck)
	s.Router.HandleFunc("/recipient/{ID}", s.Handler.GetRecipient).Methods(http.MethodGet)
	s.Router.HandleFunc("/recipient", s.Handler.CreateRecipient).Methods(http.MethodPut)
	s.Router.HandleFunc("/recipient", s.Handler.GetAllRecipients).Methods(http.MethodGet)
	s.Router.HandleFunc("/recipient/{ID}/{lastNotified}", s.Handler.GetRecipient).Methods(http.MethodPost)
	s.Router.HandleFunc("/recipient/{ID}", s.Handler.DeleteRecipient).Methods(http.MethodDelete)
}
