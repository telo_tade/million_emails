package inspector

import (
	"time"

	"github.com/go-log/log"
	"github.com/streadway/amqp"
)

const queueName = "million_emails"

// Client encapsulates inspector functionality.
type Client struct {
	L         log.Logger
	QueueURLs []string
}

func (c *Client) createConnections() map[string]*amqp.Connection {
	result := make(map[string]*amqp.Connection)

	for i := 0; i < len(c.QueueURLs); i++ {
		conn, err := amqp.Dial(c.QueueURLs[i])
		if err != nil {
			c.L.Logf("Error creating connection for %v: %v", c.QueueURLs[i], err.Error())

			result[c.QueueURLs[i]] = nil
		} else {
			result[c.QueueURLs[i]] = conn
		}
	}

	return result
}

func (c *Client) closeConnections(input map[string]*amqp.Connection) {
	for _, v := range input {
		if v != nil {
			v.Close()
		}
	}
}

func (c *Client) createChannels(input map[string]*amqp.Connection) map[string]*amqp.Channel {
	result := make(map[string]*amqp.Channel)

	for k, v := range input {
		if v == nil {
			continue
		}

		amqpChannel, err := v.Channel()
		if err != nil {
			c.L.Logf("Error creating channel for connection %v", k)

			result[k] = nil
		} else {
			result[k] = amqpChannel
		}
	}

	return result
}

func (c *Client) closeChannels(input map[string]*amqp.Channel) {
	for _, v := range input {
		if v != nil {
			v.Close()
		}
	}
}

func (c *Client) declareQueue(input map[string]*amqp.Channel) map[string]*amqp.Queue {
	result := make(map[string]*amqp.Queue)

	for k, v := range input {
		if v == nil {
			continue
		}

		q, err := v.QueueDeclare(queueName, true, false, false, false, nil)
		if err != nil {
			c.L.Logf("Error declaring queue: %v", err.Error())

			result[k] = nil
		} else {
			result[k] = &q
		}
	}

	return result
}

// Execute inspecting the queues.
func (c *Client) Execute() {
	start := time.Now()
	defer c.logDuration(start)
	c.L.Logf("Starting to execute inspector cmd: %v", start)

	connections := c.createConnections()
	defer c.closeConnections(connections)

	amqpChannels := c.createChannels(connections)
	defer c.closeChannels(amqpChannels)

	for {
		queues := c.declareQueue(amqpChannels)

		for k, v := range queues {
			if v == nil {
				c.L.Logf("\nURL %v has no active queue!\n", k)
			} else {
				c.L.Logf("\nQueue %v, URL %v has:\n		%d consumers and %d messages\n",
					v.Name, k, v.Consumers, v.Messages)
			}
		}

		time.Sleep(1 * time.Second)
	}
}

func (c *Client) logDuration(start time.Time) {
	c.L.Logf("Started at %v, It took %v", start, time.Since(start))
}
