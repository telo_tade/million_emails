package email_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/million_emails/pkg/email"
	"gitlab.com/telo_tade/million_emails/pkg/models"
)

func TestClient_SendEmail(t *testing.T) {
	type fields struct {
		from        string
		failureRate int
		timeoutRate int
	}

	type args struct {
		email models.Email
	}

	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "5% failure, 1% timeout",
			args: args{
				email: models.Email{Body: "test body", Subject: "test subject", To: "test@gmail.com"},
			},
			fields: fields{from: "test.sender@gmail.com", failureRate: 5, timeoutRate: 1},
		},
		{
			name: "100% failure, 0% timeout",
			args: args{
				email: models.Email{Body: "test body", Subject: "test subject", To: "test@gmail.com"},
			},
			fields: fields{from: "test.sender@gmail.com", failureRate: 100, timeoutRate: 0},
		},
		{
			name: "0% failure, 100% timeout",
			args: args{
				email: models.Email{Body: "test body", Subject: "test subject", To: "test@gmail.com"},
			},
			fields: fields{from: "test.sender@gmail.com", failureRate: 0, timeoutRate: 100},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c, err := email.New(tt.fields.failureRate, tt.fields.timeoutRate, tt.fields.from)
			assert.Nil(t, err, "Err should be nil.")

			start := time.Now()
			err = c.SendEmail(tt.args.email)

			switch {
			case time.Since(start) > 5*time.Second:
				assert.NotNil(t, err, "Err should not be nil.")
				assert.Equal(t, "Timeout when sending email from "+tt.fields.from, err.Error(), "Wrong error message.")
			case err != nil:
				assert.Equal(t, "Failed to send email from "+tt.fields.from, err.Error(), "Wrong error message.")
			}
		})
	}
}
