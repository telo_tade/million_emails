package email

import (
	"math/rand"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/telo_tade/million_emails/pkg/models"
)

// Client can be used to mock email sending.
type Client struct {
	from        string
	failureRate int
	timeoutRate int
}

// New instantiates a new client.
func New(failureRate, timeoutRate int, from string) (*Client, error) {
	if failureRate < 0 || failureRate > 100 {
		return nil, errors.Errorf("Failure rate %d should be between 0 and 100", failureRate)
	}

	if timeoutRate < 0 || timeoutRate > 100 {
		return nil, errors.Errorf("Timeout rate %d should be between 0 and 100", timeoutRate)
	}

	return &Client{
		failureRate: failureRate,
		timeoutRate: timeoutRate,
		from:        from,
	}, nil
}

// SendEmail simulates sending an email.
func (c *Client) SendEmail(email models.Email) (err error) {
	if !email.Valid() {
		return errors.Errorf("Invalid email.")
	}

	scenario := rand.Intn(100)

	switch {
	case scenario < c.failureRate: // failure
		time.Sleep(500 * time.Millisecond)

		return errors.Errorf("Failed to send email from %v", c.from)
	case scenario < c.failureRate+c.timeoutRate: // timeout
		time.Sleep(5 * time.Second)

		return errors.Errorf("Timeout when sending email from %v", c.from)
	default: // success
		time.Sleep(500 * time.Millisecond)

		return nil
	}
}
