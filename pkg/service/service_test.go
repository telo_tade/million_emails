package service_test

import (
	"context"
	"reflect"
	"testing"
	"time"

	"github.com/go-log/log"
	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/million_emails/pkg/models"
	"gitlab.com/telo_tade/million_emails/pkg/service"
)

func TestService_GetRecipient(t *testing.T) {
	type fields struct {
		Logger   log.Logger
		DBClient *dbClientMock
	}

	type args struct {
		ctx         context.Context
		RecipientID uint
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		want   models.Recipient
	}{
		{
			name: "GetRecipient called once",
			args: args{
				ctx:         context.Background(),
				RecipientID: 12,
			},
			fields: fields{
				Logger:   t,
				DBClient: &dbClientMock{},
			},
			want: models.Recipient{ID: 12, Email: "test@gmail.com", Name: "John Doe"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.Service{
				Logger:   tt.fields.Logger,
				DBClient: tt.fields.DBClient,
			}
			got, err := s.GetRecipient(tt.args.ctx, tt.args.RecipientID)

			assert.Nil(t, err, "Error should be nil.")
			assert.Equal(t, 1, tt.fields.DBClient.getRecipient, "GetRecipient should be called only once.")
			assert.True(t, reflect.DeepEqual(tt.want, *got), "Wrong returned Recipient.")
		})
	}
}

type dbClientMock struct {
	getRecipient, lastNotified int
}

func (d *dbClientMock) GetRecipient(ctx context.Context, id uint) (models.Recipient, error) {
	d.getRecipient++

	return models.Recipient{ID: 12, Email: "test@gmail.com", Name: "John Doe"}, nil
}

func (d *dbClientMock) GetAllRecipients(ctx context.Context) ([]models.Recipient, error) {
	return []models.Recipient{}, nil
}

func (d *dbClientMock) CreateRecipient(ctx context.Context, p models.Recipient) error {
	return nil
}

func (d *dbClientMock) UpdateLastNotified(ctx context.Context, recipientID int, deletionAt time.Time) error {
	d.lastNotified++

	return nil
}

func (d *dbClientMock) DeleteRecipient(ctx context.Context, r models.Recipient) error {
	return nil
}

func TestService_UpdateLastNotified(t *testing.T) {
	type fields struct {
		Logger   log.Logger
		DBClient *dbClientMock
	}

	type args struct {
		ctx          context.Context
		RecipientID  int
		LastNotified time.Time
	}

	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "UpdateLastNotified called once",
			args: args{
				ctx:         context.Background(),
				RecipientID: 12,
			},
			fields: fields{
				Logger:   t,
				DBClient: &dbClientMock{},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.Service{
				Logger:   tt.fields.Logger,
				DBClient: tt.fields.DBClient,
			}

			err := s.UpdateLastNotified(tt.args.ctx, tt.args.RecipientID, tt.args.LastNotified)

			assert.Nil(t, err, "Error should be nil.")
			assert.Equal(t, 1, tt.fields.DBClient.lastNotified, "UpdateLastNotified should be called only once.")
		})
	}
}
