package service

import (
	"context"
	"time"

	"github.com/go-log/log"
	"gitlab.com/telo_tade/million_emails/pkg/models"
)

// Service hold the business logic.
type Service struct {
	Logger   log.Logger
	DBClient dbClient
}

// dbClient models interaction with DB.
type dbClient interface {
	GetRecipient(ctx context.Context, id uint) (models.Recipient, error)
	GetAllRecipients(ctx context.Context) ([]models.Recipient, error)
	CreateRecipient(ctx context.Context, p models.Recipient) error
	UpdateLastNotified(ctx context.Context, recipientID int, deletionAt time.Time) error
	DeleteRecipient(ctx context.Context, r models.Recipient) error
}

// CreateRecipient creates a new recipient.
func (s *Service) CreateRecipient(ctx context.Context, r models.Recipient) error {
	err := s.DBClient.CreateRecipient(ctx, r)
	if err != nil {
		s.Logger.Logf("Error creating recipient: %v.", err)

		return err
	}

	return nil
}

// DeleteRecipient deletes an existing recipient.
func (s *Service) DeleteRecipient(ctx context.Context, recipientID int) error {
	r, err := s.DBClient.GetRecipient(ctx, uint(recipientID))
	if err != nil {
		s.Logger.Logf("Error: recipient does not exist %v.", err)

		return err
	}

	err = s.DBClient.DeleteRecipient(ctx, r)
	if err != nil {
		s.Logger.Logf("Error deleting recipient: %v.", err)

		return err
	}

	return nil
}

// GetRecipient returns the recipient having a given ID.
func (s *Service) GetRecipient(ctx context.Context, recipientID uint) (*models.Recipient, error) {
	recipient, err := s.DBClient.GetRecipient(ctx, recipientID)
	if err != nil {
		s.Logger.Logf("Error getting recipient from db, msg: %v.", err)

		return nil, err
	}

	return &recipient, nil
}

// GetAllRecipients returns all recipients.
func (s *Service) GetAllRecipients(ctx context.Context) ([]models.Recipient, error) {
	recipients, err := s.DBClient.GetAllRecipients(ctx)
	if err != nil {
		s.Logger.Logf("Error getting all recipients %v.", err)

		return nil, err
	}

	return recipients, nil
}

// UpdateLastNotified updates a given recipient's lastNotified field.
func (s *Service) UpdateLastNotified(ctx context.Context, recipientID int, notifiedAt time.Time) error {
	err := s.DBClient.UpdateLastNotified(ctx, recipientID, notifiedAt)
	if err != nil {
		s.Logger.Logf("Error updating last notified for recipienID %v, msg: %v.", recipientID, err.Error())
	}

	return err
}
