package generator_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/million_emails/pkg/generator"
	"gitlab.com/telo_tade/million_emails/pkg/models"
)

type dbMock struct {
	insert, largest int
}

func (d *dbMock) InsertBulk(recipients []models.Recipient) error {
	d.insert++

	return nil
}

func (d *dbMock) GetLargestID(ctx context.Context) (int, error) {
	d.largest++

	return 0, nil
}

func TestClient_Generate(t *testing.T) {
	type fields struct {
		DB    dbMock
		Count int
	}

	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "Should call GetLargestID and InsertBulk",
			fields: fields{
				Count: 1000,
				DB:    dbMock{},
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := generator.Client{
				DB:    &tt.fields.DB,
				Count: tt.fields.Count,
			}

			err := c.Generate()
			assert.Nil(t, err, "Err should be nil.")
			assert.Equal(t, 1, tt.fields.DB.insert, "InsertBulk should have been called once.")
			assert.Equal(t, 1, tt.fields.DB.largest, "GetLargestID should have been called once.")
		})
	}
}
