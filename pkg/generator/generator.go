package generator

import (
	"context"
	"fmt"

	"gitlab.com/telo_tade/million_emails/pkg/models"
)

// Client encapsulates functionality to add recipients in DB.
type Client struct {
	DB    database
	Count int
}

type database interface {
	InsertBulk(recipients []models.Recipient) error
	GetLargestID(ctx context.Context) (int, error)
}

// Generate will add c.Count new recipients into DB.
func (c *Client) Generate() error {
	largest, err := c.DB.GetLargestID(context.Background())
	if err != nil {
		return err
	}
	largest++

	recipients := make([]models.Recipient, c.Count)
	for i := 0; i < c.Count; i++ {
		recipients[i] = models.Recipient{
			ID:    largest + i,
			Email: fmt.Sprintf("user_%v@mockserver.com", largest+i),
			Name:  fmt.Sprintf("name_%v", largest+i),
		}
	}

	return c.DB.InsertBulk(recipients)
}
