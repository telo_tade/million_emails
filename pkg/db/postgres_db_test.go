package db_test

import (
	"context"
	"reflect"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/million_emails/pkg/db"
	"gitlab.com/telo_tade/million_emails/pkg/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func createDBClientMock() (gdb *gorm.DB, mock sqlmock.Sqlmock, err error) {
	handler, mock, err := sqlmock.New()
	if err != nil {
		return nil, nil, err
	}

	gdb, err = gorm.Open(postgres.New(postgres.Config{
		DriverName:           "mocked_postgres",
		DSN:                  "",
		PreferSimpleProtocol: false,
		Conn:                 handler,
	}), &gorm.Config{})
	if err != nil {
		return nil, nil, err
	}

	return gdb, mock, nil
}

func TestNewClientNoConnection(t *testing.T) {
	client, err := db.NewClient("a", 1, "b", "c", "d")
	assert.Nil(t, client, "Client should  be nil.")
	assert.NotNil(t, err, "Error should not be nil.")
}

func TestCreateRecipient(t *testing.T) {
	gdb, mock, err := createDBClientMock()
	assert.Nil(t, err, "Error should be nil.")

	timeMock := time.Date(2020, 8, 18, 22, 12, 1, 11, time.UTC)
	p := models.Recipient{
		ID:           95,
		Name:         "mcdr",
		LastNotified: timeMock,
		Email:        "foo@example.net",
	}
	rows := sqlmock.NewRows([]string{"id"}).AddRow(95)

	mock.ExpectBegin()
	mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "recipients" ("name","email","last_notified","id") VALUES ($1,$2,$3,$4) RETURNING "id"`)).
		WithArgs(p.Name, p.Email, p.LastNotified, p.ID).
		WillReturnRows(rows)

	mock.ExpectCommit()

	client := db.PostgresClient{DB: gdb}
	err = client.CreateRecipient(context.Background(), p)
	assert.Nil(t, err, "Error should be nil.")

	err = mock.ExpectationsWereMet()
	assert.Nil(t, err, "Error should be nil.")
}

func TestGetRecipient(t *testing.T) {
	gdb, mock, err := createDBClientMock()
	assert.Nil(t, err, "Error should be nil")

	now := time.Date(2020, 8, 18, 22, 12, 1, 11, time.UTC)
	expected := models.Recipient{
		ID:    12,
		Name:  "Tobias",
		Email: "tobias@metronom",
	}
	rows := mock.NewRows(
		[]string{"id", "name", "deletion_at", "email"},
	).AddRow(expected.ID, expected.Name, now, expected.Email)

	mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "recipients" WHERE id = $1`)).
		WithArgs(12).
		WillReturnRows(rows)

	client := db.PostgresClient{DB: gdb}
	p, err := client.GetRecipient(context.Background(), 12)
	assert.Nil(t, err, "Error should be nil.")
	assert.True(t, reflect.DeepEqual(expected, p), "Wrong recipient value.")

	err = mock.ExpectationsWereMet()
	assert.Nil(t, err, "Error should be nil.")
}

func TestUpdateRecipient(t *testing.T) {
	gdb, mock, err := createDBClientMock()
	assert.Nil(t, err, "Error should be nil.")

	p := models.Recipient{
		ID:    95,
		Name:  "mcdr",
		Email: "tobias@metronom",
	}

	mock.ExpectBegin()
	mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "recipients" SET "id"=$1,"name"=$2,"email"=$3 WHERE "id" = $4`)).
		WillReturnResult(sqlmock.NewResult(95, 1)).
		WithArgs(p.ID, p.Name, p.Email, p.ID)
	mock.ExpectCommit()

	client := db.PostgresClient{DB: gdb}

	err = client.UpdateRecipient(context.Background(), p)
	assert.Nil(t, err, "Error should be nil.")

	err = mock.ExpectationsWereMet()
	assert.Nil(t, err, "Error should be nil.")
}

func TestDeleteRecipient(t *testing.T) {
	gdb, mock, err := createDBClientMock()
	assert.Nil(t, err, "Error should be nil.")

	mock.ExpectBegin()
	mock.ExpectExec(regexp.QuoteMeta(
		`DELETE FROM "recipients" WHERE "recipients"."id" = $1`)).
		WithArgs(95).
		WillReturnResult(sqlmock.NewResult(95, 1))
	mock.ExpectCommit()

	client := db.PostgresClient{DB: gdb}
	p := models.Recipient{
		ID:    95,
		Name:  "mcdr",
		Email: "tobias@metronom",
	}

	err = client.DeleteRecipient(context.Background(), p)
	assert.Nil(t, err, "Error should be nil.")

	err = mock.ExpectationsWereMet()
	assert.Nil(t, err, "Error should be nil.")
}

func TestGetAllRecipients(t *testing.T) {
	gdb, mock, err := createDBClientMock()
	assert.Nil(t, err, "error should be nil.")

	expected := []models.Recipient{{
		ID:    95,
		Name:  "Tobias",
		Email: "tobias@metronom",
	}, {
		ID:    95,
		Name:  "Markus",
		Email: "Markus@metronom",
	}}
	rows := mock.NewRows(
		[]string{"id", "name", "email"},
	).
		AddRow(expected[0].ID, expected[0].Name, expected[0].Email).
		AddRow(expected[1].ID, expected[1].Name, expected[1].Email)

	mock.ExpectQuery(regexp.QuoteMeta(`SELECT * FROM "recipients"`)).
		WithArgs().
		WillReturnRows(rows)

	client := db.PostgresClient{DB: gdb}
	res, err := client.GetAllRecipients(context.Background())
	assert.Nil(t, err, "Error should be nil.")

	assert.True(t, reflect.DeepEqual(expected, res), "Wrong recipient value.")

	err = mock.ExpectationsWereMet()
	assert.Nil(t, err, "Error should be nil.")
}

func TestUpdateLastNotified(t *testing.T) {
	gdb, mock, err := createDBClientMock()
	assert.Nil(t, err, "Error should be nil")

	timeMock := time.Date(2020, 8, 18, 22, 12, 1, 11, time.UTC)

	mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "recipients" SET "last_notified"=$1 WHERE id = $2`)).
		WithArgs(timeMock, 12).
		WillReturnResult(sqlmock.NewResult(12, 1))

	client := db.PostgresClient{DB: gdb}
	err = client.UpdateLastNotified(context.Background(), 12, timeMock)
	assert.Nil(t, err, "Error should be nil.")

	err = mock.ExpectationsWereMet()
	assert.Nil(t, err, "Error should be nil.")
}
