package db

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
	funk "github.com/thoas/go-funk"
	"gitlab.com/telo_tade/million_emails/pkg/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

const (
	timeToWait      = 1 * time.Hour
	insertBulkSize  = 500
	insertQuery     = `INSERT INTO "recipients" ("name","email","last_notified","id") VALUES %s RETURNING "id"`
	layout          = "2006-01-02 15:04:05"
	updateBulkQuery = `UPDATE recipients SET last_notified='%s' WHERE id IN (%s)`
)

// PostgresClient holds the internal connection to the database.
type PostgresClient struct {
	DB *gorm.DB
}

// NewClient establishes an initial connection to the database and sets up the schema.
func NewClient(host string, port uint, username string, password string, database string) (*PostgresClient, error) {
	con, err := createConnection(host, port, username, password, database)
	if err != nil {
		return nil, err
	}

	result := &PostgresClient{DB: con}

	if err := result.migrateDB(); err != nil {
		return nil, err
	}

	return result, nil
}

// migrateDB sets up the schema. No data will be deleted/transformed during this process.
func (c *PostgresClient) migrateDB() error {
	err := c.DB.AutoMigrate(&models.Recipient{})
	if err != nil {
		return errors.Wrap(err, "Could not create the schema")
	}

	return nil
}

// createConnection establishes the connection with the PostgreSQL DB.
func createConnection(host string, port uint, username string, password string, database string) (*gorm.DB, error) {
	db, err := gorm.Open(postgres.Open(fmt.Sprintf("host=%v port=%v user=%v password=%v dbname=%v sslmode=disable",
		host, port, username, password, database)), &gorm.Config{})
	if err != nil {
		return nil, errors.Wrap(err, "Could not establish the DB connection")
	}

	return db, nil
}

// CreateRecipient adds a new recipient to the DB.
func (c *PostgresClient) CreateRecipient(ctx context.Context, r models.Recipient) error {
	res := c.DB.WithContext(ctx).Create(&r)
	if res.Error != nil {
		return errors.Wrap(res.Error, "Could not create a user")
	}

	return nil
}

// GetRecipient retrieves a recipient from the DB based on the supplied recipient ID.
func (c *PostgresClient) GetRecipient(ctx context.Context, id uint) (models.Recipient, error) {
	var r models.Recipient

	res := c.DB.WithContext(ctx).Where("id = ?", id).Find(&r)
	if res.Error != nil || res.RowsAffected == 0 {
		return models.Recipient{}, errors.New(fmt.Sprintf("Could not find a recipient with ID %v.", id))
	}

	return r, nil
}

// UpdateRecipient updates a given recipient entry inside the DB.
func (c *PostgresClient) UpdateRecipient(ctx context.Context, r models.Recipient) error {
	res := c.DB.WithContext(ctx).Model(r).Updates(r)
	if res.Error != nil {
		return errors.Wrap(res.Error, "Could not update the recipient")
	}

	return nil
}

// DeleteRecipient deletes a given recipient entry from the DB.
func (c *PostgresClient) DeleteRecipient(ctx context.Context, r models.Recipient) error {
	res := c.DB.WithContext(ctx).Delete(r)
	if res.Error != nil {
		return errors.Wrap(res.Error, "Could not delete the recipient")
	}

	return nil
}

// GetAllRecipients retrieves all recipients from the database.
func (c *PostgresClient) GetAllRecipients(ctx context.Context) ([]models.Recipient, error) {
	var r []models.Recipient

	res := c.DB.WithContext(ctx).Find(&r)
	if res.Error != nil {
		return nil, errors.Wrap(res.Error, "Could not find any recipients")
	}

	return r, nil
}

// UpdateLastNotified updates a given recipient's lastNotified field.
func (c *PostgresClient) UpdateLastNotified(ctx context.Context, recipientID int, notifiedAt time.Time) error {
	res := c.DB.WithContext(ctx).Model(models.Recipient{}).
		Where("id = ?", recipientID).Update("last_notified", notifiedAt)
	if res.Error != nil {
		return errors.Wrap(res.Error, "could not update last_notified of recipient")
	}

	return nil
}

// InsertBulk writes multiple recipients at once.
func (c *PostgresClient) InsertBulk(recipients []models.Recipient) error {
	tx := c.DB.Begin()

	chunkList := funk.Chunk(recipients, insertBulkSize)

	for _, chunk := range chunkList.([][]models.Recipient) {
		valueStrings := []string{}
		valueArgs := []interface{}{}

		for _, recipient := range chunk {
			valueStrings = append(valueStrings, "(?, ?, ?, ?)")

			valueArgs = append(valueArgs, recipient.Name)
			valueArgs = append(valueArgs, recipient.Email)
			valueArgs = append(valueArgs, recipient.LastNotified.Format(layout))
			valueArgs = append(valueArgs, strconv.Itoa(recipient.ID))
		}

		stmt := fmt.Sprintf(insertQuery, strings.Join(valueStrings, ","))

		err := tx.Exec(stmt, valueArgs...).Error
		if err != nil {
			tx.Rollback()

			return err
		}
	}

	err := tx.Commit().Error
	if err != nil {
		return err
	}

	return nil
}

// GetLargestID returns the largest recipient ID found in our DB.
func (c *PostgresClient) GetLargestID(ctx context.Context) (int, error) {
	var r models.Recipient

	res := c.DB.WithContext(ctx).Order("id DESC").Limit(1).Find(&r)
	if res.Error != nil || res.RowsAffected == 0 {
		return 0, errors.New("could not find any recipients")
	}

	return r.ID, nil
}

// GetNextUnnotified retrieves all recipients from the database having:
//	 IDs between startID and stopID and last_notified older than 1 hour.
func (c *PostgresClient) GetNextUnnotified(ctx context.Context, startID, stopID, max int) ([]models.Recipient, error) {
	var r []models.Recipient

	res := c.DB.WithContext(ctx).
		Where("id > ? and id < ?", startID, stopID).
		Where("last_notified < ?", time.Now().Add(-1*timeToWait)).
		Limit(max).Find(&r)
	if res.Error != nil {
		return nil, errors.Wrap(res.Error, "Could not find any recipients")
	}

	return r, nil
}

// UpdateLastNotifiedBulk updates the LastNotified field of multiple recipients at once.
func (c *PostgresClient) UpdateLastNotifiedBulk(ids []string, when time.Time) error {
	stmt := fmt.Sprintf(updateBulkQuery, when.Format(layout), strings.Join(ids, ","))
	tx := c.DB.Begin()

	err := tx.Exec(stmt).Error
	if err != nil {
		tx.Rollback()

		return err
	}

	err = tx.Commit().Error
	if err != nil {
		return err
	}

	return nil
}
