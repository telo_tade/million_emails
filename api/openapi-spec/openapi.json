{
    "openapi": "3.0.0",
    "info": {
      "title": "REST API µS",
      "description": "This API offers a way manipulate Recipient objects.\n",
      "version": "1.0.0",
      "contact": {
        "name": "Liviu Esanu",
        "email": "liviuadrian.esanu@metronom.com"
      }
    },
    "servers": [
      {
        "url": "https://www.example.com"
      }
    ],
    "paths": {
      "/recipient/{ID}": {
        "parameters": [
          {
            "in": "path",
            "name": "ID",
            "schema": {
              "type": "integer"
            },
            "required": true,
            "description": "ID of the desired Recipient."
          }
        ],
        "get": {
          "summary": "Get Recipient with given ID.",
          "description": "Search and retrive the Recipient with given ID, if it exists.",
          "tags": [
            "Recipients"
          ],
          "responses": {
            "200": {
              "description": "A Recipient with the given ID.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "object",
                    "minItems": 0,
                    "description": "The Recipient.",
                    "items": {
                      "$ref": "#/components/schemas/Recipient"
                    }
                  }
                }
              }
            },
            "400": {
              "description": "Invalid request."
            }
          }
        },
        "delete": {
          "summary": "Delete the recipient with given ID.",
          "description": "Deletes the Recipient with given ID, if it exists.",
          "tags": [
            "Recipients"
          ],
          "responses": {
            "200": {
              "description": "Delete was successful."
            },
            "400": {
              "description": "Invalid request."
            }
          }
        }
      },
      "/recipient/{ID}/{lastNotified}": {
        "parameters": [
          {
            "in": "path",
            "name": "ID",
            "schema": {
              "type": "integer"
            },
            "required": true,
            "description": "ID of the desired Recipient."
          },
          {
            "in": "path",
            "name": "lastNotified",
            "schema": {
              "type": "number"
            },
            "required": true,
            "description": "Date when this recipient was last notified."
          }
        ],
        "post": {
          "summary": "Update lastNotified.",
          "description": "Update the lastNotified field of recipient with given ID, if it exists.",
          "tags": [
            "Recipients"
          ],
          "responses": {
            "200": {
              "description": "LastNotified field was updated successfuly."
            },
            "400": {
              "description": "Invalid request."
            }
          }
        }
      },
      "/recipient": {
        "put": {
          "summary": "Create a new Recipient.",
          "tags": [
            "Recipients"
          ],
          "parameters": [
            {
              "in": "query",
              "name": "clusters",
              "schema": {
                "type": "object",
                "items": {
                  "$ref": "#/components/schemas/Recipient"
                }
              },
              "required": true,
              "description": "The Recipient value."
            }
          ],
          "responses": {
            "200": {
              "description": "Recipient was successfully created.",
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/Recipient"
                  }
                }
              }
            },
            "500": {
              "description": "Error adding Recipient."
            }
          }
        },
        "get": {
          "summary": "Get all Recipients.",
          "tags": [
            "Recipients"
          ],
          "responses": {
            "200": {
              "description": "Recipient were retrieved successfully.",
              "content": {
                "application/json": {
                  "schema": {
                    "type": "array",
                    "items": {
                      "$ref": "#/components/schemas/Recipient"
                    }
                  }
                }
              }
            },
            "400": {
              "description": "Invalid request."
            }
          }
        }
      },
      "/ping": {
        "get": {
          "summary": "Report if service is up and running.",
          "tags": [
            "misc"
          ],
          "responses": {
            "200": {
              "description": "Service is up and running.",
              "content": {
                "text": {
                  "schema": {
                    "type": "string",
                    "example": "pong"
                  }
                }
              }
            }
          }
        }
      }
    },
    "components": {
      "schemas": {
        "Recipient": {
          "type": "object",
          "properties": {
            "ID": {
              "type": "integer",
              "description": "The Recipient ID.",
              "example": "123"
            },
            "Name": {
              "type": "string",
              "description": "The Recipient name.",
              "example": "John Doe"
            },
            "Email": {
              "type": "string",
              "description": "Recipient email.",
              "example": "john.doe@gmail.com"
            }
          }
        }
      }
    }
}
