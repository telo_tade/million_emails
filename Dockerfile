# Dockerfile References: https://medium.com/@chemidy/create-the-smallest-and-secured-golang-docker-image-based-on-scratch-4752223b7324
FROM golang:1.15 AS builder
# Add Maintainer Info
LABEL maintainer="Liviu Esanu <liviuadrian.esanu@metronom.com>"

# Install git.
# Git is required for fetching the dependencies.
WORKDIR $GOPATH/src/gitlab.com/telo_tade/million_emails

COPY . .
 
# Build the binary
RUN GIT_COMMIT=$(git rev-list -1 HEAD) && \
  VERSION=$(git describe --tags $(git rev-list --tags --max-count=1)) && \
  GO111MODULE=on CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -mod vendor \
  -ldflags "-X main.GitCommit=$GIT_COMMIT -X main.Version=$VERSION" \
  -o cmd/main/main \
  cmd/main/main.go

# Prepare certificates
FROM alpine:latest as certs
RUN apk --update add ca-certificates

############################
# STEP 2 build a small image
############################
FROM scratch
LABEL maintainer="Liviu Esanu <liviuadrian.esanu@metronom.com>"

COPY --from=certs /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt

# Copy our static executable. 
COPY --from=builder /go/src/gitlab.com/telo_tade/million_emails/cmd/main/main /bin/
COPY --from=builder /go/src/gitlab.com/telo_tade/million_emails/config/config.yaml /config/

# Run the binary.
ENTRYPOINT ["/bin/main"]