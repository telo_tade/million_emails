// +build integration

package integration

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestExample(t *testing.T) {
	assert.Equal(t, 1, 1, "Values are not equal.")
}
