# Send emails to one  million users problem

This software attempts to send email to a large list of users. The users are stored in a DB, and each email takes 0.5 seconds to send. No recipient can accidentally get 2 emails. Some of the flows are documented in the docs folder.

## My idea

One million recipients is a large enough number to start thinking about scaling issues, but imagine we needed to send personalized emails to one thousand million recipients. The flow for each email would be read the recipient from a DB, add it to a message queue, take it from that message queue and issue a call to an email service provider:

### 1) Storing email recipients

* I would store the data about each of these one thousand recipients in around 50 databases
  * ensure a roughly equal amount of data in each database by distributing it using a hash function, or an algorithms like "all emails starting with letter a are stored in this DB"
  * create a µS offering a REST API to manipulate the recipients (wrapping CRUD operation over a DB table)
  * have a few instances of this API µS, under a loadbalancer
  * make this API µS decide to which DB each recipient belongs to, make it have a connection ofr each DB

### 2) Read a recipient from DB, compose its personalized email and add that to a message queue

* For each DB there are a few message queues and an order of magnitude more Producer µServices
* Each producer µS:
  * has a connection to only one DB
  * it reads from that DB a batch of 1000 recipients
  * it updates, in that DB, a last_notified field for every recipient it has read
  * it composes personalized emails for each of those 1000 recipients
  * it distributes those 1000 messages to its pool of message queues

### 3) Message queues for each DB

* They store the personalized emails
* Each producer µS has a pool of several queues in which it puts data
* Each consumer µS only reads (and sometimes puts back) data only from(to) a single queue

### 4) Consume a message from a queue and deliver it

* For each queue there are several Consumer µS instances
* Every consumer µS instance:
  * reads a few messages, acknowledges them then tries to deliver them
  * each failed to deliver message is placed back in that queue, with a counter field increased
  * if a message failed to be delivered enough times:
    * it is no longer placed back in the queue but discarded
    * a HTTP RESTS call will be made to the API µS to remove the corresponding recipient

## Assumptions

* It does not send any email: the email sending operation is simply mocked.
* Each email has only one recipient, no CC or multiple recipients.
* Sending one email can have three outcomes:
  * success: this is simulated by waiting 0.5 seconds
  * failure: this is simulated by waiting 0.5 seconds and returning an error
  * timeout failure: this is simulating by waiting 5 seconds and returning an error
* Failure and timeout rates are configurable
* As the email addresses of recipients are stored in DB, this enforces that each recipient address is unique
* Recipient management is done via a REST API
* There is no priority difference between recipients: once starting to send email we cannot enforce that email for recipient X will be sent sooner than email for recipient Y
* There is no email domain or SMTP validation, email addresses are assumed to be correct

## How to run

This µS uses subcomands, and thus can run in multiple modes. To solve the problem we would have multiple databases and queues, as well as multiple instances of this app concurrently runnning in producer and consumer mode. Consumer mode needs a dedicated queue, and ideally we will have many consumers for every queue.

Generator mode:
go run cmd/main/main.go -config="path to config file"  generate

Api mode:
go run cmd/main/main.go -config="path to config file" api

Producer mode:
go run cmd/main/main.go -config="path to config file" produce

Inspector mode:
go run cmd/main/main.go -config="path to config file" inspect

Consumer mode:
go run cmd/main/main.go -config="path to config file" consume

## How to tie it up all together

First install postgreSQL, create a data base, and fill in the config values inside /config/configs.yaml. Start it in generator mode, and that will add one million recipients into your DB. Then install rabbitMQ locally, fill in the config values with its URL, and start the application in produce mode. This will add a message into the message queue for every recipient. Lastly, start the app in consumer mode, preferable many consumers for the same queue, and wait until the consumer retrieves all emails from your message queue and mock delivers them.

Whyle running consumer / producer make sure to also run this app in inspect mode. This will print infor about the queue contents, every second.

## What I did not solve

Due to large input data, many things that are simplistic now raise additional challenges. For example, when trying to read all the recipients having their ID between startID and stopID, the DB is running and new Receivers are being created. These new ones will not be emailed, it they were created with an ID larger than the configured stopID. I have simply ignored many of these issues.
On top of that, I make no attempt to delete a recipient whose emails repeatedly fail to be delivered. If an email fails to be delivered it is simply placed back into the queue, however it should only be placed a few times, after that simply delete it and its recipient.

